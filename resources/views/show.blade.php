@extends('layouts.index')
@section('content')

<div class="section">
	<div class="card-panel black darken-3 white-text center">POLITEKNIK BATUNUNGGAL BANDUNG POL B-B</div>
</div>

<div class="section">
	@foreach($datas as $data)

	<div class="row">
		<div class="col s12">
			<h5>{{ $data->judul }}</h5>

            <div class="divider"></div>
            <p>{!!substr($data->isi,0,200)!!}...</p>

            <a href="{{ url('read', $data->id) }}" class="btn btn-flat pink accent-3 waves-effect waves-light white-text">Readmore <i class="material-icons right">send</i></a>

		</div>
	</div>
	@endforeach

</div>




{{ $datas->render() }}
@endsection
