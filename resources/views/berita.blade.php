@extends('layouts.index')
@section('content')

<div class="container">
  <div class="page-header">
    <h1 class="page-title">
      Berita
    </h1>
  </div>
  <div class="row row-cards row-deck">
		@foreach($datas as $data)
    <div class="col-sm-6 col-xl-3">
      <div class="card">
        <a href="{{ route('read', $data->id) }}"><img class="card-img-top" src="{{ asset('image/'.$data->gambar)  }}"></a>
        <div class="card-body d-flex flex-column">
          <h4><a href="{{ route('read', $data->id) }}">{{ $data->judul }}</a></h4>
          <div class="text-muted">{{ substr($data->isi,0,200) }}...</div>
          <div class="d-flex align-items-center pt-5 mt-auto">
            <div class="avatar avatar-md mr-3" style="background-image: url(./demo/faces/female/18.jpg)"></div>
            <div>
              <a href="./profile.html" class="text-default">Rose Bradley</a>
              <small class="d-block text-muted">12 - 09 - 2017</small>
            </div>
            <div class="ml-auto text-muted">
							@guest
							@else
							<a href="{{ route('deleteBerita', $data->id) }}"
                                              onclick="return confirm('are you sure?')"
                                                       class="icon d-none d-md-inline-block ml-3"><i class="fe fe-trash mr-1"></i></a>
							@endguest
            </div>
          </div>
        </div>
      </div>
    </div>
		@endforeach
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			{{ $datas->render() }}
		</div>
	</div>
</div>
@endsection
