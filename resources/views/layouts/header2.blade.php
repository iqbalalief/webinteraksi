<nav class="black accent-3" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">WEB POL B-B</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="{{ url('/')}}">Jadwal</a></li>
          <li><a href="{{ url('/berita')}}">Berita</a></li>
            <li><a href="{{ url('/')}}">Logout</a></li>
      </ul>
<ul class="right hide-on-med-and-down">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre> <font color='white'>
                                    {{ Auth::user()->name }}        <span class="caret"></span>
                                </font> </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> <font color='white'>
                                            Logout
                                        </font> </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
      <ul id="nav-mobile" class="side-nav">
        <li><a href="{{ url('/')}}"></a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons"></i></a>
    </div>
  </nav>
