<!-- <nav class="black accent-3" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">WEB POL B-B</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="{{ url('/')}}">Home</a></li>
          <li><a href="{{ url('/berita')}}">Berita</a></li>
		  <li><a href="{{ url('/editberita')}}">Input Berita</a></li>
		  <li><b>||</b></li>
		  <li><ul class="right hide-on-med-and-down">
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre> <font color='white'>
                                    {{ Auth::user()->name }}        <span class="caret"></span>
                                </font> </a>


                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                            </li>
                            <li><a href="{{ route('logout') }}"
                                                            onclick="event.preventDefault();
                                                                     document.getElementById('logout-form').submit();" style="color:#FFF;">Logout </a></li>
                        @endguest
                    </ul></li>

      </ul>

      <ul id="nav-mobile" class="side-nav">
        <li><a href="{{ url('/')}}"></a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons"></i></a>
    </div>
  </nav> -->
  <div class="header py-4">
          <div class="container">
            <div class="d-flex">
              <a class="header-brand" href="#">
                <img src="{{ asset('assets/images/lpkia.jpg') }}" class="header-brand-img" alt="tabler logo">
              </a>
              <div class="d-flex order-lg-2 ml-auto">
                <div class="dropdown">
                  @guest
                    <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                      <!-- <span class="avatar" style="background-image: url(./demo/faces/female/25.jpg)"></span> -->
                      <span class="ml-2 d-none d-lg-block">
                        <span class="text-default">Login</span>
                        <!-- <small class="text-muted d-block mt-1">Administrator</small> -->
                      </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      <a class="dropdown-item" href="{{ route('login') }}">
                        <i class="dropdown-icon fe fe-user"></i> Login
                      </a>
                      <a class="dropdown-item" href="{{ route('register') }}">
                        <i class="dropdown-icon fe fe-settings"></i> Register
                      </a>
                    </div>
                  @else
                    <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                      <span class="avatar" style="background-image: url({{ asset('assets/images/iqbal.jpg') }})"></span>
                      <span class="ml-2 d-none d-lg-block">
                        <span class="text-default">{{ Auth::user()->name }}</span>
                        <!-- <small class="text-muted d-block mt-1">Administrator</small> -->
                      </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      <a class="dropdown-item" href="#">
                        <i class="dropdown-icon fe fe-user"></i> Profile
                      </a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#" href="{{ route('logout') }}"
                                                      onclick="event.preventDefault();
                                                               document.getElementById('logout-form').submit();">
                        <i class="dropdown-icon fe fe-log-out"></i> Sign out
                      </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                    </div>
                  @endguest
                </div>
              </div>
              <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
              </a>
            </div>
          </div>
        </div>
        <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg-3 ml-auto">
                <!-- <form class="input-icon my-3 my-lg-0">
                  <input type="search" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
                  <div class="input-icon-addon">
                    <i class="fe fe-search"></i>
                  </div>
                </form> -->

            			@guest
            			@else
            			<a href="{{ route('inputBerita') }}" class="nav-link"> Input Berita</a>
            			@endguest
              </div>
              <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                  <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link active"><i class="fe fe-home"></i> Home</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('getBerita') }}" class="nav-link"><i class="fe fe-file"></i> Berita</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-calendar"></i> Jadwal</a>
                    <div class="dropdown-menu dropdown-menu-arrow">
                      <a href="./maps.html" class="dropdown-item ">Kuliah</a>
                      <a href="./icons.html" class="dropdown-item ">Acara</a>
                      <a href="./store.html" class="dropdown-item ">PMB</a>
                      <!-- <a href="./blog.html" class="dropdown-item active">Blog</a>
                      <a href="./carousel.html" class="dropdown-item ">Carousel</a> -->
                    </div>
                  </li>
                  <li class="nav-item dropdown">
                    <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-file"></i> Pages</a>
                    <div class="dropdown-menu dropdown-menu-arrow">
                      <a href="./profile.html" class="dropdown-item ">Profile</a>
                      <a href="./login.html" class="dropdown-item ">Login</a>
                      <a href="./register.html" class="dropdown-item ">Register</a>
                      <a href="./forgot-password.html" class="dropdown-item ">Forgot password</a>
                      <a href="./400.html" class="dropdown-item ">400 error</a>
                      <a href="./401.html" class="dropdown-item ">401 error</a>
                      <a href="./403.html" class="dropdown-item ">403 error</a>
                      <a href="./404.html" class="dropdown-item ">404 error</a>
                      <a href="./500.html" class="dropdown-item ">500 error</a>
                      <a href="./503.html" class="dropdown-item ">503 error</a>
                      <a href="./email.html" class="dropdown-item ">Email</a>
                      <a href="./empty.html" class="dropdown-item ">Empty page</a>
                      <a href="./rtl.html" class="dropdown-item ">RTL mode</a>
                    </div>
                  </li>
                  <li class="nav-item dropdown">
                    <a href="./form-elements.html" class="nav-link"><i class="fe fe-check-square"></i> Forms</a>
                  </li>
                  <li class="nav-item">
                    <a href="./gallery.html" class="nav-link"><i class="fe fe-image"></i> Gallery</a>
                  </li>
                  <li class="nav-item">
                    <a href="./docs/index.html" class="nav-link"><i class="fe fe-file-text"></i> Documentation</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
