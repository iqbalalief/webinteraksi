@extends('layouts.index')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <!-- <div class="card-header">
          <h3 class="card-title">Carousel with captions</h3>
        </div> -->
        <div class="card-body">
          <div id="carousel-captions" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-100" alt="" src="{{ asset('assets/demo/photos/home1.jpg') }}" data-holder-rendered="true">
                <div class="carousel-item-background d-none d-md-block"></div>
                <div class="carousel-caption d-none d-md-block">
                  <h3>POL BB</h3>
                  <p>Politeknik Batununggal Bandung</p>
                  <p>BACA BERITA SELENGKAPNYA</p>
                  <p>----------------------</p>
                  <p>Silahkan Registrasi jika belom memiliki akun!</p>
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" alt="" src="{{ asset('assets/demo/photos/home2.jpg') }}" data-holder-rendered="true">
                <div class="carousel-item-background d-none d-md-block"></div>
                <div class="carousel-caption d-none d-md-block">
                  <h3>POL BB</h3>
                  <p>Politeknik Batununggal Bandung</p>
                  <p>BACA BERITA SELENGKAPNYA</p>
                  <p>----------------------</p>
                  <p>Silahkan Registrasi jika belom memiliki akun!</p>
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" alt="" src="{{ asset('assets/demo/photos/berkualitas.jpg') }}" data-holder-rendered="true">
                <div class="carousel-item-background d-none d-md-block"></div>
                <div class="carousel-caption d-none d-md-block">
                  <h3>POL BB</h3>
                  <p>Politeknik Batununggal Bandung</p>
                  <p>BACA BERITA SELENGKAPNYA</p>
                  <p>----------------------</p>
                  <p>Silahkan Registrasi jika belom memiliki akun!</p>
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" alt="" src="{{ asset('assets/demo/photos/PARTNER.jpg') }}" data-holder-rendered="true">
                <div class="carousel-item-background d-none d-md-block"></div>
                <div class="carousel-caption d-none d-md-block">
                  <h3>POL BB</h3>
                  <p>Politeknik Batununggal Bandung</p>
                  <p>BACA BERITA SELENGKAPNYA</p>
                  <p>----------------------</p>
                  <p>Silahkan Registrasi jika belom memiliki akun!</p>
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" alt="" src="{{ asset('assets/demo/photos/22.jpg') }}" data-holder-rendered="true">
                <div class="carousel-item-background d-none d-md-block"></div>
                <div class="carousel-caption d-none d-md-block">
                  <h3>POL BB</h3>
                  <p>Politeknik Batununggal Bandung</p>
                  <p>BACA BERITA SELENGKAPNYA</p>
                  <p>----------------------</p>
                  <p>Silahkan Registrasi jika belom memiliki akun!</p>
                </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carousel-captions" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-captions" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
