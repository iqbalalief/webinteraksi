@extends('layouts.index')
@section('content')

<div class="section">
	<div class="card-panel black darken-20 white-text"><center><b><marquee direction="left" scrollamount="5" align="center">Untuk Menambah Berita Pilih Tombol Tambah Di Pojok Kanan Bawah </marquee></b></center></div>
</div>

<div class="section">
	@foreach($datas as $data)

	<div class="row">
		<div class="col s12">
		<img src="{{ asset('image/'.$data->gambar)  }}" style="max-height:300px;max-width:300px;margin-top:10px;">
			<h5>{{ $data->judul }}</h5>

            <div class="divider"></div>
            <p>{!!substr($data->isi,0,200)!!}...</p>

            
            <a href="{{ url('edit', $data->id) }}" class="btn btn-flat black darken-4 waves-effect waves-light white-text">Edit <i class="material-icons right">mode_edit</i></a>
            <a href="{{ url('delete', $data->id) }}" onclick="return confirm('Yakin mau hapus data ini sob?')" class="btn btn-flat black darken-4 waves-effect waves-light white-text">Delete <i class="material-icons right">delete</i></a>
		</div>
	</div>
	@endforeach

</div>


<div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">

    <a href="{{ url('add')}}" class="btn-floating btn-large red">
    <i class="large material-icons">add</i>
    </a>
  </div>

{{ $datas->render() }}
@endsection
