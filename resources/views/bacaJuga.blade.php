<div class="col-lg-4">
  <div class="row">
    <div class="col-md-12">
      <h2 class="btn btn-block btn-primary mb-6">Baca Juga</h2>
    </div>
		@foreach($datas as $data)
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body d-flex flex-column">
          <h4><a href="{{ route('read', $data->id) }}">{{ $data->judul }}</a></h4>
          <div class="text-muted">{{ substr($data->isi,0,200) }}...</div>
          <div class="d-flex align-items-center pt-5 mt-auto">
            <div class="avatar avatar-md mr-3" style="background-image: url(./demo/faces/male/4.jpg)"></div>
            <div>
              <a href="./profile.html" class="text-default">Bobby Knight</a>
              <small class="d-block text-muted">3 days ago</small>
            </div>
            <div class="ml-auto text-muted">
              @guest
							@else
							<a href="{{ route('deleteBerita', $data->id) }}"
                                              onclick="return confirm('are you sure?')"
                                                       class="icon d-none d-md-inline-block ml-3"><i class="fe fe-trash mr-1"></i></a>
							@endguest
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
