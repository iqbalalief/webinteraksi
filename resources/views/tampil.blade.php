@extends('layouts.index')
@section('content')
<div class="container">
  <!-- <div class="page-header">
    <h1 class="page-title">
      Documentation
    </h1>
  </div> -->
  <div class="row">
    <div class="col-lg-8">
      <div class="card">
        <div class="card-body">
          <div class="text-wrap p-lg-6">
            <h2 class="mt-0 mb-4">{{ $tampilkan->judul }}</h2>
						<img src="{{ asset('image/'.$tampilkan->gambar)  }}" class="img-responsive">
						<br><br>
            <p>{{$tampilkan->isi}}</p>
          </div>
        </div>
      </div>
    </div>

	    @include('bacajuga')
  </div>
</div>
@endsection
