@extends('layouts.index')
@section('content')
<div class="container">
    <div class="row">
      <div class="col-lg-8">
        <form class="card" action="{{ url('store') }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="card-body">
            <h3 class="card-title">Tambah Berita</h3>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label">Foto</label>
                  <!-- <input type="text" class="form-control" name="judul"> -->
                  <input type="file" id="sgowgambar" name="gambar">
                  <!-- <div class="invalid-feedback">Invalid feedback</div> -->
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label">Judul</label>
                  <input type="text" class="form-control" name="judul">
                  <!-- <div class="invalid-feedback">Invalid feedback</div> -->
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group mb-0">
                  <label class="form-label">Isi Berita</label>
                  <textarea rows="5" class="form-control" name="isi"></textarea>
                  <!-- <div class="invalid-feedback">Invalid feedback</div> -->
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer text-right">
            <button type="submit" class="btn btn-primary">Simpan Berita</button>
          </div>
        </form>
      </div>

      @include('bacajuga')
  </div>
</div>
@endsection
