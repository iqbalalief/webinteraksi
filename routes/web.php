<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/', 'HomeController@index')->name('home');
 Auth::routes();

 Route::get('/editberita', 'CrudController@Editberita');

 Route::get('/berita', 'CrudController@index')->name('getBerita');

 Route::get('/add', 'CrudController@create')->name('inputBerita');

 Route::post('/store', 'CrudController@store');

 Route::get('/read/{id}', 'CrudController@show')->name('read');

 Route::get('/edit/{id}', 'CrudController@edit');

 Route::post('/update/{id}', 'CrudController@update');

 Route::get('/delete/{id}', 'CrudController@destroy')->name('deleteBerita');

 Route::get('/blog', 'BlogController@index');

Route::get('/home', 'HomeController@index')->name('home');
