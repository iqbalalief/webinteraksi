<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crud extends Model
{
    protected $table = 'crud';
	protected $primaryKey ='id';
	protected $fillable = ['gambar','judul', 'isi'];
	public $timestamps = false;
}
