<?php

namespace App\Http\Controllers;

Use App\Crud;
// use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class CrudController extends Controller
{

	public function editberita()
    {
        $datas = Crud::orderBy('id','DESC')->paginate(3);
      return view('editberita')->with('datas',$datas);
    }

	public function utama()
    {
      $datas = Crud::orderBy('id','DESC')->paginate(4);
      return view('show')->with('datas',$datas);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = Crud::orderBy('id','DESC')->paginate(4);
	     return view('berita', ['datas' => $datas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datas = Crud::orderBy('id','DESC')->paginate(2);
        return view('add', ['datas' => $datas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
    	   'gambar' => 'required',
          'judul' => 'required',
          'isi' => 'required'
    	]);

     $tambah = new Crud();
     $file       = $request->file('gambar');
     $fileName   = $file->getClientOriginalName();
     $request->file('gambar')->move("image/", $fileName);

	   $tambah->gambar = $fileName;
     $tambah->judul = $request['judul'];
     $tambah->isi = $request['isi'];
     $tambah->save();

     return redirect()->route('getBerita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $tampilkan = Crud::find($id);
       $datas = Crud::orderBy('id','DESC')->paginate(2);
        return view('tampil', ['tampilkan' => $tampilkan, 'datas' => $datas]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tampiledit = Crud::where('id', $id)->first();
        return view('edit')->with('tampiledit', $tampiledit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Crud::where('id', $id)->first();
		$update->gambar = $request['gambar'];
        $update->judul = $request['judul'];
        $update->isi = $request['isi'];
        $update->update();

        return redirect()->to('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $hapus = Crud::find($id);
       $hapus->delete();

       return back();
    }
}
