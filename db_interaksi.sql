-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2018 at 05:22 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_interaksi`
--

-- --------------------------------------------------------

--
-- Table structure for table `crud`
--

CREATE TABLE `crud` (
  `id` int(10) UNSIGNED NOT NULL,
  `gambar` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `crud`
--

INSERT INTO `crud` (`id`, `gambar`, `judul`, `isi`) VALUES
(7, 'dosen-itb-kembangkan-pemanis-alami-rendah-kalori-dari-daun-stevia-1r1DdLyDil.jpg', 'Dosen ITB Kembangkan Pemanis Alami Rendah Kalori dari Daun Stevia', 'JAKARTA – Tim peneliti dari staf dosen Sekolah Farmasi Institut Teknologi Bandung (SF-ITB) mengembangkan pemanis alami dari daun Stevia. Tim peneliti terdiri dari Rahmana Emran Kartasasmita, Elfahmi, Muhammad Insanu, dan As’ari Nawawi. Lengkapnya, mereka melakukan penelitian dengan topik “Pengembangan Proses Produksi Pemanis Alami Glikosida Steviol (Stevia) dari Daun Tanaman Stevia.”\r\n\r\nLebih lanjut, para dosen tersebut memilih daun dan herba tanaman Stevia, karena mengandung senyawa yang memiliki rasa manis glikosida steviol atau GS, dengan kadar cukup tinggi. Kemanisan senyawa GS mencapai 300 kali lipat dari gula, sehingga GS yang diperoleh melalui ektraksi daun dan herba tanaman Stevia ini banyak digunakan sebagai pemanis alami pensubstitusi gula, khususnya bagi yang memerlukan asupan kalori rendah. GS juga tidak memiliki nilai kalori sehingga tidak akan menyebabkan kenaikan gula darah setelah dikonsumsi. Oleh karena itu, pemanis alami ini sesuai untuk digunakan oleh penderita diabetes maupun yang memerlukan asupan kalori rendah seperti yang sedang melakukan diet rendah kalori.'),
(8, 'pengumuman-unsri.png', 'ITB Sediakan 1.636 Kuota Mahasiswa di Jalur SBMPTN 2018', 'JAKARTA – Tim peneliti dari staf dosen Sekolah Farmasi Institut Teknologi Bandung (SF-ITB) mengembangkan pemanis alami dari daun Stevia. Tim peneliti terdiri dari Rahmana Emran Kartasasmita, Elfahmi, Muhammad Insanu, dan As’ari Nawawi. Lengkapnya, mereka melakukan penelitian dengan topik “Pengembangan Proses Produksi Pemanis Alami Glikosida Steviol (Stevia) dari Daun Tanaman Stevia.”\r\n\r\nLebih lanjut, para dosen tersebut memilih daun dan herba tanaman Stevia, karena mengandung senyawa yang memiliki rasa manis glikosida steviol atau GS, dengan kadar cukup tinggi. Kemanisan senyawa GS mencapai 300 kali lipat dari gula, sehingga GS yang diperoleh melalui ektraksi daun dan herba tanaman Stevia ini banyak digunakan sebagai pemanis alami pensubstitusi gula, khususnya bagi yang memerlukan asupan kalori rendah. GS juga tidak memiliki nilai kalori sehingga tidak akan menyebabkan kenaikan gula darah setelah dikonsumsi. Oleh karena itu, pemanis alami ini sesuai untuk digunakan oleh penderita diabetes maupun yang memerlukan asupan kalori rendah seperti yang sedang melakukan diet rendah kalori.'),
(9, '1527946222.jpg', 'Generasi Milenial Harus Siap Menghadapi Perubahan', 'Bandung - Pada era globalisasi yang semakin kompetitif, generasi muda dituntut untuk terus berinovasi dan mampu mengimbangi kemajuan teknologi yang begitu pesat. Kaum muda yang dikenal dengan istilah generasi milenial itu juga harus mampu memanfaatkan perubahan zaman menjadi potensi untuk menggali kreatifitas yang dimiliki.\r\n\r\n\"Anak muda tidak boleh takut dengan perubahan, tetapi menjadikan perubahan sebagai peluang meraih masa depan,\" ujar Ketua Sarinah Partai Demokrasi Indonesia Perjuangan (PDIP) Jawa Barat (Jabar), Ika Eviolina Hasanuddin, saat menjadi pembicara dalam seminar bertema \"Smart and Kreatif\" di Gedung Serba Guna LPKIA Bandung, Jabar, Sabtu (2/6).\r\n\r\nMantan artis sinetron itu mengatakan, generasi milenial juga harus lebih kuat menghadapi berbagai tantangan ke depan dalam meraih impian serta membawa perubahan yang lebih baik bagi pribadi maupun bangsa. Dikatakan, upaya meraih kesuksesan tidak bisa dilakukan dengan cara instan, melainkan melalui proses kerja keras dan tekad yang kuat.\r\n\r\n\"Perjuangan itu tanpa henti. Impian itu tanpa batas. Bukan hasilnya yang penting, tetapi bagaimana proses perjalannya yang harus kita hargai,\" ujar istri calon gubernur Jabar nomor urut dua, Tb Hasanuddin itu.\r\n\r\nSementara itu, Ketua DPRD Jabar, Ineu Purwadewi, yang juga menjadi pembicara mengatakan, generasi muda juga harus siap menjadi pemimpin di masa depan. Anak muda, lanjut Ineu, tidak boleh cepat berpuas diri dalam menggali pengetahuan lebih luas.\r\n\r\nMenurut dia, salah satu modal penting bagi seorang pemimpin masa depan adalah wawasan luas dan tidak berhenti belajar. \"Kepemimpinan itu lahir dari pelatihan. Hidup itu tidak pernah berhenti dari belajar. Belajar itu harus terus dilakukan hingga siap memimpin,\"ujar politisi PDIP itu.\r\n\r\nIneu memaparkan, dunia saat ini begitu dinamis dan cepat berubah. Setiap individu harus mampu menyesuaikan diri terhadap perkembangan teknologi, gaya hidup, maupun sosial dan budaya. Ineu menegaskan, dengan perubahan yang ada, anak muda diuji untuk mampu memanfaatkan hal tersebut, salah satunya media sosial (Medsos).\r\n\r\nDikatakan, medsos saat ini penting bagai seorang pemimpin dalam menyosialisasikan berbagai program pemerintah serta menginformasikan progres pembangunan daerah kepada masyarakat.\r\n\r\n\"Memanfaatkan media sosial sangat penting pada era saat ini. Mengelola kepimpinan juga bisa dilakukan melalui media sosial,\" tuturnya.\r\n\r\n\r\n\r\nSumber: Suara Pembaruan'),
(10, 'pohon-tumbang-di-dekat-lpkia_20160127_180748.jpg', 'Hujan Deras Disertai Angin, Tumbangkan Pohon Di Dekat Kampus LPKIA', 'BANDUNG, TRIBUNJABAR.CO.ID - Sebatang pohon tumbang mengarah ke jalan membuat kemacetan di Jalan Soekarno-Hatta, Bandung, Rabu (27/1/2016).\r\n\r\nLokasi tepatnya di dekat Kampus LPKIA dari arah Cibiru menuju perempatan Moh Toha.\r\nPohon tersebut tumbang saat terjadi hujan deras disertai angin kencang yang melanda sebagian Kota Bandung.\r\n\r\nKondisi tersebut membuat khawatir sebagian pengendara baik motor maupun mobil yang tengah berada dalam perjalanan.\r\n\r\nSementara itu Dinas Pencegahan dan Penanggulangan Kebakaran (DPPK) seperti dilansir RESCUE DAMKAR BDG @Rescue_Damkar di media sosial Twitter, menyebutkan sekitar pukul 17.00 telah mengirimkan tim rescue ke lokasi kejadian.\r\n\r\nHingga berita ini dilaporkan belum diketahui akibat dari tumbangnya pohon tersebut. (dd/*)\r\n\r\n\r\n\r\nArtikel ini telah tayang di tribunjabar.id dengan judul Hujan Deras Disertai Angin, Tumbangkan Pohon di Dekat Kampus LPKIA, http://jabar.tribunnews.com/2016/01/27/hujan-deras-disertai-angin-tumbangkan-pohon-di-dekat-kampus-lpkia.\r\n\r\nEditor: Dedy Herdiana'),
(13, 'bursa-kerja-di-lpkia.jpg', 'Masih Menganggur? Coba Deh ke Career Day LPKIA, Siapa Tahu Beruntung   Artikel ini telah tayang di tribunjabar.id dengan judul Masih Menganggur? Coba Deh ke Career Day LPKIA, Siapa Tahu Beruntung', 'BANDUNG, TRIBUN - Lembaga pendidikan LPKIA menggelar Bursa Tenaga Kerja \" Career Day\" di Kampus LPKIA Jalan Soekarno-Hatta, Sabtu (24/1). Pada penyelenggaraan kali ini, LPKIA mengambil tema \"Go Fight Win\"‎.\r\n\r\nSebanyak 82 stand yang berasal dari berbagai perusahaan dari berbagai bidang seperti asuransi, perbankan, industri, ritel, hingga otomotif ikut dalam kegiatan kali ini. Pengunjung yang mayoritas mahasiswa LPKIA‎ bisa bertemu langsung untuk melamar atau mendapat informasi terkait pekerja yang dibutuhkan oleh perusahaan tersebut.\r\n\r\nHadir pada acara Kepala Dinas Tenaga Kerja dan Transmigrasi (Disnakertrans) Jawa Barat Hening Widiatmoko, Direktur LPKIA Paulus Tamzil, dan Kepala Bursa Tenaga Kerja LPKIA Jenifer Yunani‎. (tif)\r\n\r\n\r\n\r\nArtikel ini telah tayang di tribunjabar.id dengan judul Masih Menganggur? Coba Deh ke Career Day LPKIA, Siapa Tahu Beruntung, http://jabar.tribunnews.com/2015/01/24/masih-menganggur-coba-deh-ke-career-day-lpkia-siapa-tahu-beruntung.\r\nPenulis: Siti Fatimah	\r\nEditor: Kisdiantoro'),
(15, '021656700_1526295618-550665081_750x422.jpg', 'Di Usia 92 Tahun, Wanita Ini Lulus Kuliah untuk Keempat Kalinya', 'Liputan6.com, Jakarta - Momen kelulusan atau wisuda adalah sungguh momen yang tak bernilai dan tak terlupakan. Selain Anda terkenang akan kerja keras Anda selama kuliah, Anda juga terharu mengingat perjuangan orangtua dalam membiayai pendidikan Anda. \r\n\r\nFaktanya, tidak semua momen wisuda hanya dikhususkan bagi orang-orang muda saja. Di luar negeri misalnya, seorang pemilik salon diketahui baru lulus menempuh sarjana saat ia berusia 92 tahun. \r\n\r\nDilansir dari CNBC, Rabu (16/5/2018) dialah Annie Dillard seorang pemilik salon di Carolina Selatan yang lulus ketika berusia 92 tahun. Namun jangan salah sangka dulu, ini merupakan kali keempat bagi dirinya untuk lulus kuliah. \r\n\r\n\"Mereka bilang saya untuk datang pukul 05:30 pagi saat wisuda ini, namun saya tentu akan datang on time yakni saya datang pukul 4 pagi. Perasaan ini sungguh sangat luar biasa, saya benar-benar tidak sabar,\" tuturnya. \r\n\r\nDillard menambahkan bahwa melanjutkan pendidikan membantunya untuk senantiasa tajam dalam mengingat. Ia juga menginspirasi orang lain saat berbicara dunia pendidikan.'),
(16, '051027400_1526377771-iStock-926247184.jpg', 'Keren, Bocah di Belgia Masuk Kuliah Usia 8 Tahun', 'Liputan6.com, Brussels - Keren, seorang bocah asal Belgia lulus sekolah menengah di usia delapan tahun. Sebelumnya, ia menyelesaikan pendidikan dasar hanya dalam kurun waktu 1,5 tahun.\r\n\r\nSeperti dikutip dari BBC, Minggu (1/7/2018), bocah jenius itu diketahui bernama Laurent Simons. Sang ayah orang Belgia dan ibunya berasal dari Belanda.\r\n\r\nMenurut orang tuanya, Laurent Simons memiliki nilai Intelligence Quotient (IQ) 145. Ia akan masuk pendidikan diploma, yang umumnya memiliki murid dengan usia 18 tahun.\r\n\r\nBerbicara kepada radio RTBF Belgia, Laurent mengatakan subjek favoritnya adalah matematika. \"Karena begitu luas, ada statistik, geometri, aljabar,\" kata Laurent Simons.\r\n\r\nSang ayah mengatakan putranya memiliki kesulitan berinteraksi untuk bermain dengan anak-anak lain ketika masih balita. Putranya juga tak terlalu tertarik pada mainan. Tak disangka ternyata buah hatinya itu justru memiliki kepandaian yang luar biasa.\r\n\r\nBocah Belgia itu mengatakan dia bercita-cita menjadi seorang ahli bedah dan astronot. Tetapi kini ia tertarik dengan dunia komputer.\r\n\r\n\"Jika dia memutuskan besok untuk menjadi tukang kayu, itu tak jadi masalah bagi kami, selama dia bahagia,\" tutur sang ayah.'),
(17, '054973500_1526051598-P_20180511_182912.jpg', 'Tertarik Kerja di Bidang Big Data? Ini Jurusan Kuliah yang Dicari', 'Liputan6.com, Jakarta - Data merupakan salah satu elemen utama di era Revolusi Industri 4.0. Tak pelak bila big data turut menjadi tren menarik untuk didalami, baik itu oleh sektor industri maupun para generasi muda yang ingin berkecimpung di dalamnya.\r\n\r\nDi Indonesia sendiri sudah berdiri perusahaan analisis big data bernama Bigjava yang menjalin kerja sama dengan sektor pemerintahan maupun swasta. Ruli Harjowidianto, CEO Bigjava, menyebut penggunaan big data dapat memberikan keuntungan besar di beragam sektor.\r\n\r\nLantas, bidang studi mana yang kemampuannya memegang peranan penting di bidang ini? Ternyata lulusan matematika menjadi primadona di area big data.\r\n\r\nApa yang diungkap Ruli dapat membuat orang-orang heran, sebab orang awam pasti mengira lulusan berkaitan ilmu komputer yang justru dicari untuk bidang big data.\r\n\r\n\"Karena kalau kita ambil lulusan komputer, mereka belum tentu tahu big data, kedua ia juga harus belajar big data lagi, lalu belum tentu juga dia suka big data,\" jelasnya.\r\n\r\n\"Tapi kalau lulusan matematika, mereka memikirkan pemakaian logika untuk mengejar tujuan, dan mereka bisa belajar programming. Sekarang yang banyak dipakai lulusan matematika untuk menjadi data scientist (ilmuwan data), karena yang mereka kerjakan analisis dan logika. Kemampuan analisisnya yang dicari dan bagaimana menjadikan logika tersebut agar dimengerti orang lain,\" lanjutnya\r\n\r\nRuli juga menerangkan lulusan matematika memiliki kemampuan mumpuni untuk menyusun rumus atau algoritma yang dibutuhkan di bidang big data.\r\n\r\n\"Kami punya data scientist di New York. Gelarnya doktor, tiga orang, lulusan matematika semua,\" ungkap Ruli.'),
(18, 'bd7e685dd8b24d5f320384f80256cf94ng-book.jpg', 'Remaja Ini Lolos Seleksi Beasiswa di 20 Perguruan Tinggi AS', 'Liputan6.com, Texas - Seorang remaja pria dari negara bagian Texas dikabarkan berhasil diterima kuliah di 20 perguruan tinggi di Amerika Serikat.\r\n\r\nMenariknya, seluruh perguruan tinggi tersebut menerima dengan tawaran beasiswa penuh. Hal itu membuat kedua orang tuanya merasa kagum, sekaligus bangga.\r\n\r\n\"Saya sangat bersyukur, saya sangat berterima kasih,\" ujar ibunya, Berthinia Rutledge-Brown, sebagaimana dikutip dari ABC News pada Rabu (5/4/2018),\r\n\r\n\"Saya tahu bahwa dia telah melakukan pekerjaan luar biasa, dan saya menyaksikan hal itu setiap hari. Dia mendapatkan lebih dari yang ia harapkan,\" lanjutnya bangga.\r\n\r\nBeberapa di antara universitas tersebut bahkan termasuk dalam kategori pergurua tinggi paling bergengsi di negeri Paman Sam, seperti Stanford, Georgetown, Priceton, Yale, dan Harvard.\r\n\r\nOrang tua Brown menggambarkan dirinya sebagai sosok \'pekerja keras\' dan \'penuh perhatian\' terhadap orang-orang di sekitarnya.\r\n\r\n\"Dia ingin ini menjadi contoh dan inspirasi bagi anak-anak lain, bahwa jika mereka bekerja keras dan menggunakan kemampuan sendiri, mereka dapat melakukan apa pun yang mereka inginkan di masa depan,\" kata Rutledge-Brown.\r\n\r\nBrown memiliki tenggat waktu memilih universitas yang dikehendakinya hingga awal Mei mendatang, sebelum siap menempuh bangku kuliah setelah libur musim panas.');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_07_02_125750_create_crud_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'iqbalaliefya', 'iqbalaliefyaeldhyawan@gmail.com', '$2y$10$2scZLOMAlpjLIC..Qg4uHu2QNoyOnSKh0FoPlT./fuubAQLb9DRWu', 'kJZVCoLjjflBnteyw7laVA5jFAVkQRBg6hcAI0aEjUD3XiJD7bGeSBSHh8x6', '2018-07-09 07:24:48', '2018-07-09 07:24:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `crud`
--
ALTER TABLE `crud`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `crud`
--
ALTER TABLE `crud`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
